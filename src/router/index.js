import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

import Bull from '../views/Bull.vue'
import Canards from '../views/Canards.vue'
import Chatter from '../views/Chatter.vue'
import Whispers from '../views/Chinesewhispers.vue'
import Gabfest from '../views/Gabfest.vue'
import MudSling from '../views/Mud.vue'
import PowWow from '../views/PowWow.vue'
import Scuttlebutt from '../views/Scuttlebutt.vue'
import Smear from '../views/Smear.vue'
import Tittle from '../views/Tittle.vue'
import Television from '../views/Television.vue'
import Radio from '../views/Radio.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },

  {
    path: '/bull',
    name: 'Bull Session',
    component: Bull
  },

  {
    path: '/canards',
    name: 'Canards',
    component: Canards
  },

  {
    path: '/chatter',
    name: 'Chatter',
    component: Chatter
  },

  {
    path: '/radio',
    name: 'Rádio',
    component: Radio
  },

  {
    path: '/television',
    name: 'Televisão',
    component: Television
  },

  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
